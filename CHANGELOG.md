# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.5.0](https://github.com/tmorin/ff-cpdn/compare/v1.4.0...v1.5.0) (2020-08-16)


### Features

* **#2:** display a UI notification when cleaning is done ([b442dbf](https://github.com/tmorin/ff-cpdn/commit/b442dbf1d8e35fe264d1eb501719b58c26ea7801)), closes [#2](https://github.com/tmorin/ff-cpdn/issues/2)
* **ui:** display popup on browser action ([eebbc72](https://github.com/tmorin/ff-cpdn/commit/eebbc729649beb64db7e1af5545ccd03cbbeaf1b))


### Bug Fixes

* skip icons because no refresh ([fba06f5](https://github.com/tmorin/ff-cpdn/commit/fba06f5c72b1b47af5b50c9aaf012ea0c0e2fe91))

# [1.4.0](https://github.com/tmorin/ff-cpdn/compare/v1.3.2...v1.4.0) (2019-03-31)


### Features

* clean all cookies related to the tab's URL whatever the cookie' path ([ffe63b6](https://github.com/tmorin/ff-cpdn/commit/ffe63b6))



## [1.3.2](https://github.com/tmorin/ff-cpdn/compare/v1.3.1...v1.3.2) (2019-03-26)


### Bug Fixes

* contextual entities are not managed ([e757137](https://github.com/tmorin/ff-cpdn/commit/e757137))



<a name="1.3.1"></a>
## [1.3.1](https://github.com/tmorin/ff-cpdn/compare/v1.3.0...v1.3.1) (2018-12-18)


### Bug Fixes

* service workers were not properly unregistered ([f67bf68](https://github.com/tmorin/ff-cpdn/commit/f67bf68))



<a name="1.3.0"></a>
# [1.3.0](https://github.com/tmorin/ff-cpdn/compare/v1.2.0...v1.3.0) (2018-12-06)


### Features

* clear entries added using the Cache API ([a07785d](https://github.com/tmorin/ff-cpdn/commit/a07785d))



<a name="1.2.0"></a>
# [1.2.0](https://github.com/tmorin/ff-cpdn/compare/v1.1.0...v1.2.0) (2018-07-18)


### Features

* clean sessionStorage ([03d8923](https://github.com/tmorin/ff-cpdn/commit/03d8923))
